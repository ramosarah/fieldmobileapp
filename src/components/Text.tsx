import React from 'react';
import {
  Text as OriginalText,
  TextProps as OriginalTextProps,
  StyleSheet,
} from 'react-native';
import { typography, Colors } from '/theme';

export interface TextProps extends OriginalTextProps {
  italic?: boolean;
  uppercase?: boolean;
  bold?: boolean;
  h1?: boolean;
  h2?: boolean;
  h3?: boolean;
  h4?: boolean;
  h5?: boolean;
  h6?: boolean;
  p?: boolean;
  orange?: boolean;
  black?: boolean;
  white?: boolean;
  grey?: boolean;
  red?: boolean;
  darkgrey?: boolean;
  greyICRC?: boolean;
  numberOfLines?: number;
}

export const Text: React.FC<TextProps> = ({ style, children, ...props }) => (
  <OriginalText
    {...props}
    style={StyleSheet.flatten([
      ...getTypography(props),
      ...getColors(props),
      style,
    ])}
    numberOfLines={props.numberOfLines}>
    {children}
  </OriginalText>
);

const typographyStyles = StyleSheet.create(typography);

function getTypography(props: TextProps) {
  return [
    typographyStyles.normal,
    props.bold && typographyStyles.bold,
    props.uppercase && typographyStyles.uppercase,
    props.italic && typographyStyles.italic,
    props.p && typographyStyles.p,
    props.h6 && typographyStyles.h6,
    props.h5 && typographyStyles.h5,
    props.h4 && typographyStyles.h4,
    props.h3 && typographyStyles.h3,
    props.h2 && typographyStyles.h2,
    props.h1 && typographyStyles.h1,
  ];
}

const colorStyles = StyleSheet.create({
  black: {
    color: Colors.black,
  },
  white: {
    color: Colors.white,
  },
  grey: {
    color: Colors.grey,
  },
  red: {
    color: Colors.red,
  },
  darkgrey: {
    color: Colors.darkGrey,
  },
  greyICRC: {
    color: Colors.greyICRC,
  },
});

function getColors(props: TextProps) {
  return [
    colorStyles.black,
    props.white && colorStyles.white,
    props.grey && colorStyles.grey,
    props.red && colorStyles.red,
    props.darkgrey && colorStyles.darkgrey,
  ];
}
