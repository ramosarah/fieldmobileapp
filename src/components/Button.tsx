import React, { useCallback } from 'react';
import {
  Platform,
  TouchableOpacity,
  TouchableWithoutFeedbackProps,
  View,
  StyleProp,
  FlexStyle,
  TextStyle,
  StyleSheet,
  GestureResponderEvent,
  Keyboard,
} from 'react-native';
import { Text } from './Text';

export interface ButtonProps {
  label?: string;
  labelStyle?: StyleProp<FlexStyle | TextStyle>;
  icon?: any;
  disabled?: boolean;
  onPress?: (event: GestureResponderEvent) => void;
  style?: StyleProp<FlexStyle | TextStyle>;
  uppercase?: boolean;
  primaryButton?: boolean;
  secondaryButton?: boolean;
  iconViewStyles?: any;
  iconStyles?: any;
}

type TouchableCompoment = React.ComponentClass<TouchableWithoutFeedbackProps>;

export const Button: React.FC<ButtonProps> = ({
  icon,
  disabled,
  label,
  labelStyle,
  onPress,
  style,
  children,
  uppercase,
  primaryButton,
  secondaryButton,
  iconViewStyles,
  iconStyles,
}) => {
  const ButtonCompoment =
    Platform.select({
      ios: TouchableOpacity as TouchableCompoment,
      android: TouchableOpacity as TouchableCompoment,
    }) as TouchableCompoment;

  const handleOnPress = useCallback(
    (event: GestureResponderEvent) => {
      onPress && onPress(event);
      Keyboard.dismiss();
    },
    [onPress],
  );
  return (
    <ButtonCompoment
      style={StyleSheet.flatten([
        styles.container,
        style,
        disabled && styles.disabled,
      ])}
      disabled={disabled}
      onPress={handleOnPress}>
      <View
        style={[
          styles.content,
          secondaryButton && styles.secondaryStyle,
          primaryButton && styles.primaryStyle,
        ]}>
        {(label || children || primaryButton || secondaryButton || uppercase) &&
          getLabelComponent({
            label: label || children,
            style: labelStyle,
            primaryButton,
            secondaryButton,
            uppercase,
          })}
        {icon && (
          <View style={StyleSheet.flatten([styles.icon, iconViewStyles])}>
            <Icon icon={icon} style={iconStyles} />
          </View>
        )}
      </View>
    </ButtonCompoment>
  );
};

function getLabelComponent({
  label,
  style,
  primaryButton,
  secondaryButton,
  uppercase,
}: {
  label: string | React.ReactNode;
  style?: StyleProp<FlexStyle>;
  primaryButton?: boolean;
  secondaryButton?: boolean;
  uppercase?: boolean;
}) {
  if (typeof label === 'string') {
    return (
      <Text
        style={[
          StyleSheet.flatten([styles.label, style]),
          primaryButton && styles.primaryText,
          secondaryButton && styles.secondaryText,
          uppercase && styles.uppercaseStyle,
        ]}>
        {label}
      </Text>
    );
  }
  return label;
}

const styles = StyleSheet.create({
  container: {
    minHeight: 40,
  },
  disabled: {
    opacity: 0.3,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 4,
    justifyContent: 'center',
  },
  primaryStyle: {
    backgroundColor: Colors.red,
  },
  secondaryStyle: {
    backgroundColor: Colors.white,
    borderWidth: 1,
    borderColor: Colors.lightGray,
  },
  primaryText: {
    color: Colors.white,
    textTransform: 'uppercase',
  },
  secondaryText: {
    color: Colors.red,
    textTransform: 'uppercase',
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    width: 30,
    height: 30,
    borderRadius: 30,
  },
  label: {
    marginHorizontal: 20,
    letterSpacing: 1,
    fontWeight: 'bold',
  },
  uppercaseStyle: {
    textTransform: 'uppercase',
  },
});
