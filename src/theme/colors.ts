export const Colors = {
  red: '#CE171E',
  white: '#FFFFFF',
  darkWhite: '#F9F9F9F0',
  black: '#000000',
  grey: '#3333338A',
  darkGrey: '#777777',
  lightGrey: '#0000004D',
  gray: '#00000029',
  lightGray: '#E8E8E8',
  greyICRC: '#333333',
  redTransparent: 'rgba(206, 23, 30, 0.3)',

  beige: '#D6D2CB',
  bgIcon: '#F2F2F2',
};
