import { StyleSheet } from 'react-native';

export const commonStyles = StyleSheet.create({
  // flex orientation
  flexRow: {
    display: 'flex',
    flexDirection: 'row',
  },
  alignCenter: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerHorizontal: {
    alignItems: 'center',
  },
  flexColumn: {
    flexDirection: 'column',
  },

  alignSelfCenter: {
    alignSelf: 'center',
  },

  // paddings
  paddingTop10: {
    paddingTop: 10,
  },
  paddingTop20: {
    paddingTop: 20,
  },
  paddingBottom10: {
    paddingBottom: 10,
  },
  paddingBottom15: {
    paddingBottom: 15,
  },

  // margins
  marginTop10: {
    marginTop: 10,
  },
  marginRight10: {
    marginRight: 10,
  },

  marginHorizontal0: {
    marginHorizontal: 0,
  },
});
