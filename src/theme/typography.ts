import { scaleH } from '/helpers/responsive';

export const fonts = {
  SFProText: 'SFProText',
  tabs: 'tabs',
};

export const typography = {
  normal: {
    fontWeight: 'normal' as const,
    fontSize: scaleH(12),
    lineHeight: scaleH(16),
  },
  small: {
    fontSize: scaleH(10),
    lineHeight: scaleH(12),
  },
  italic: {
    fontStyle: 'italic' as const,
  },
  uppercase: {
    textTransform: 'uppercase' as const,
  },
  bold: {
    fontWeight: 'bold' as const,
  },
  h1: {
    fontSize: scaleH(42),
    lineHeight: scaleH(40),
    textAlignVertical: 'center' as const,
  },
  h2: {
    fontSize: scaleH(32),
    lineHeight: scaleH(30),
    textAlignVertical: 'center' as const,
  },
  h3: {
    fontSize: scaleH(28),
    lineHeight: scaleH(26),
    textAlignVertical: 'center' as const,
  },
  h4: {
    fontSize: scaleH(20),
    lineHeight: scaleH(28),
    textAlignVertical: 'center' as const,
  },
  h5: {
    fontSize: scaleH(18),
    lineHeight: scaleH(28),
    textAlignVertical: 'center' as const,
  },
  h6: {
    fontSize: scaleH(16),
    lineHeight: scaleH(24),
    textAlignVertical: 'center' as const,
  },
  p: {
    fontSize: scaleH(14),
    lineHeight: scaleH(21),
  },
};
