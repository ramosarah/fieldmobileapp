import React from 'react';
import { Navigation } from '/Navigations';


export const Router = () => {
  const authenticated = useSelector((state) => state.profile.authenticated);

  return <Navigation authenticated={authenticated} />;
};
